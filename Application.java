import java.util.Scanner;

public class Application
{
	public static void main(String args[])
	{
		boolean playing = true;
		boolean valid = false;
		Game games[] = new Game[100];
		int gamesPlayed = 0;
		int players = 0;
		String input = " ";
		int index;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to cmd UNO!");
		
		while (playing)
		{
			System.out.println("Game " + (gamesPlayed+1));
			
			System.out.println("\nHow many people will be playing today?\n");
			
			while (!valid)
			{
				try
				{
					players = scan.nextInt();
					if (players >= 2 && players <= 8)
					{
						valid = true;
					}
					else
					{
						System.out.println("\n\033[31mPlease enter a number between 2 and 8\033[0m\n");
					}
				}
				catch (Exception e)
				{
					System.out.println("\n\033[31mPlease enter a number between 2 and 8\033[0m\n");
					scan.next();
				}
			}
			valid = false;
			
			games[gamesPlayed] = new Game (players);
						
			while(!games[gamesPlayed].gameWon())
			{
				games[gamesPlayed].playTurn(games[gamesPlayed].getTurn());
				games[gamesPlayed].incrementTurn(1);
				System.out.println();
			}
			
			System.out.println("You win!");
			
			System.out.println(games[gamesPlayed]);
			
			gamesPlayed++;
			
			while (! input.equals("again"))
			{
				System.out.println("\nYou have played " + (gamesPlayed) + " games.\n");
				System.out.println("Do you wish to play again or see the score of a previous game?\nEnter the number of a game you want to see or type \"again\" to continue.\n\"quit\" will exit the program.\n");
				Scanner scnr = new Scanner(System.in);
				input = scnr.next();
				System.out.println();
				
				if (input.toLowerCase().equals("quit"))
				{
					playing=false;
					break;
				}
				else if (input.equals("again"))
				{
					input="";
					break;
				}
				else
				{
					try
					{
						index = Integer.parseInt(input);
						System.out.println(games[index-1].toString());
					}
					catch (Exception e)
					{
						System.out.println("\n\033[31mPlease enter a valid input\033[0m\n");
					}
				}
			}
		}
		
		/*
		CardList deck = new CardList();
		deck.setCards(deck.generateDeck());
		
		System.out.println(deck);
		System.out.println();
		deck.shuffle();
		System.out.println(deck);
		System.out.println();
		System.out.println(deck.numCardsLeft());
		System.out.println();
		
		CardList hand = new CardList();
		hand.setCards(deck.drawCards(7));
		System.out.println();
		System.out.println(hand);
		
		CardList playedCards = new CardList();
		
		Scanner scan = new Scanner(System.in);
		int pick;
		
		boolean turn = true;
		while(turn)
		{
			System.out.println(hand);
			System.out.println();
			pick = scan.nextInt();
			
			playedCards.addCards(hand.drawCards(pick));
			
			System.out.println(playedCards);
			System.out.println();
		}
		*/

		/*
		CardList hand2 = new CardList();
		hand2.setCards(deck.drawCards(7));
		System.out.println();
		System.out.println(hand2);
		
		
		CardList hand3 = new CardList();
		hand3.setCards(deck.drawCards(7));
		System.out.println();
		System.out.println(hand3);
		System.out.println();
		
		
		hand.addCards(hand2.drawCards(3));
		System.out.println();
		System.out.println(hand);
		System.out.println();
		System.out.println(hand2);
		System.out.println();
		
		
		System.out.println();
		System.out.println(deck);
		System.out.println();
		System.out.println(deck.numCardsLeft());
		System.out.println();
		*/
	}
}