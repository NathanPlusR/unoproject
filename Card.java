public class Card
{
	private CardColour colour;
	private CardValue value;
	
	public Card(CardColour colour, CardValue value)
	{
		this.colour = colour;
		this.value = value;
	}
	
	public CardColour getColour()
	{
		return this.colour;
	}
	
	public CardValue getValue()
	{
		return this.value;
	}
	
	public String toString()
	{
		String s = "["+colour.toString() + value.toString() + "\033[0m" + "] ";
		return s;
	}
}