public enum CardColour{
	BLUE,
	GREEN,
	RED,
	YELLOW;
	
	public String toString()
	{	
		switch (this) {
		  case BLUE:
			return "\033[34m";
		  case GREEN:
			return "\033[32m";
		  case RED:
			return "\033[31m";
		  case YELLOW:
			return "\033[33m";
		}
		
		return "\033[0m";
	}
}