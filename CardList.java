import java.util.Random;

public class CardList
{
	private Card[] deckCards;
	private int cardNum;
	
	public CardList()
	{
		this.deckCards = new Card[100];
		//4 of every 0 and 8 of every card makes 100
		this.cardNum = -1;
	}
	
	public void setCards(Card[] cards)
	{
		this.cardNum = cards.length-1;
		//this.deckCards = cards;
		
		for(int i = 0; i <= this.cardNum; i++)
		{
			this.deckCards[i] = cards[i];
		}
	}
	
	public void addCards(Card[] newCards)
	{
		if (this.deckCards[0] == null)
		{
			setCards(newCards);
		}
		else
		{
			int previousNum = this.cardNum;
			/*
			while (this.cardNum < previousNum + newCards.length)
			{
				this.deckCards[this.cardNum+1] = newCards[this.cardNum-previousNum];
				this.cardNum++;
			}
			*/
			for (int i = newCards.length-1; i >= 0; i--)
			{
				this.deckCards[this.cardNum+1] = newCards[i];
				this.cardNum++;
			}
		}
	}
	
	public void addCards(Card newCard)
	{	
		if (this.deckCards[0] == null)
		{
			Card[] newCards = {newCard};
			setCards(newCards);
		}
		else
		{
			this.cardNum++;
			this.deckCards[this.cardNum] = newCard;
		}
	}
	
	public Card[] generateDeck()
	{
		Card[] deck = new Card[100];
		//4 of every 0 and 8 of every card makes 100
		CardValue[] values = CardValue.values();
		CardColour[] colours = CardColour.values();
		
		int i = 0;
		
		while (i < deck.length)
		{
			for (int j = 0; j < values.length; j++)
			{
				for (int k = 0; k < colours.length; k++)
				{
					if (i > 3 && values[j] == CardValue.ZERO)
					{
						j ++;
						k--;
					}
					else
					{
						deck[i] = new Card(colours[k], values[j]);
						i++;
					}
				}
			}
		}
		
		return deck;
	}
	
	public String toString()
	{
		String s = "";
		for(int i = 0; i <= this.cardNum; i++)
		{
			s += this.deckCards[i];// + "\n";
		}
		return s;
	}
	
	public Card drawCardAtIndex(int index)
	{
		if (index > this.cardNum)
		{
			throw new IndexOutOfBoundsException();
		}
		
		Card returnCard;
		
		returnCard = this.deckCards[index];
		removeAtIndex(index);
		
		return returnCard;
	}
	
	public Card[] drawCards(int amount)
	{
		if (amount > this.cardNum + 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		Card[] returnCards = new Card[amount];
		
		for(int i = 0; i < amount; i++)
		{
			returnCards[i] = this.deckCards[this.cardNum];
			removeAtIndex(this.cardNum);
		}
		return returnCards;
	}
	
	public void removeAtIndex(int index)
	{
		for (int i = index; i < this.cardNum; i++)
		{
			this.deckCards[i] = this.deckCards[i+1];
		}
		this.cardNum--;
	}
	
	public void shuffle()
	{
		Random random = new Random();
		Card temp;
		int index;
		
		for(int i = 0; i <= this.cardNum; i++)
		{
			index = random.nextInt(cardNum);
			temp = this.deckCards[i];
			this.deckCards[i] = this.deckCards[index];
			this.deckCards[index] = temp;
		}
	}
	
	public Card getTopCard()
	{
		return deckCards[this.cardNum];
	}
	
	public Card getCard(int index)
	{
		return deckCards[index];
	}
	
	public int numCardsLeft()
	{
		return this.cardNum + 1;
	}
}