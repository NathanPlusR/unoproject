import java.util.Scanner;

public class Game
{
	private CardList deck;
	private CardList pile;
	private CardList[] hands;
	private CardList playedCards;
	private int turnPlace;
	private boolean ascendingTurnOrder;
	private int winner;
	private int highestCombo;
	private int highestComboer;
	
	private boolean hangingUNO;
	private int hangingPlayer;
	
	public Game(int players)
	{
		this.deck = new CardList();
		this.deck.setCards(this.deck.generateDeck());
		this.deck.shuffle();
		
		this.ascendingTurnOrder = true;
		
		this.playedCards = new CardList();
		
		this.turnPlace = 0;
		
		this.pile = new CardList();
		this.pile.addCards(this.deck.drawCards(1));
		
		this.hands = new CardList[players];
		
		for (int i = 0; i < players; i++)
		{
			this.hands[i] = new CardList();
			this.hands[i].setCards(this.deck.drawCards(7));
		}
		
		this.highestCombo = 0;
	}
	
	public void playTurn(int player)
	{
		Scanner scan = new Scanner(System.in);
		String input = "";
		int index;
		boolean unUsedUNO = true;
		System.out.println((player+1)+"'s turn!\n");
		
		while (!input.toLowerCase().equals("s"))
		{
			System.out.println("(Remember, 'i' will give you a list of commands)\n");
			
			if (this.hands[player].numCardsLeft() != 0)
			{
				System.out.println("Hand:\n"+this.hands[player]);
				printOptions(this.hands[player].numCardsLeft());
			}
			
			if (this.playedCards.numCardsLeft() != 0)
			{
				System.out.println("\nPlayed Cards in order:\n"+this.playedCards);
			}
			else
			{
				System.out.println("Pile:\n"+this.pile.getTopCard());
			}
			
			System.out.println();
			
			input = scan.next();
			System.out.println();
			
			if (input.toLowerCase().equals("s"))
			{
				boolean skippable = true;
				
				for (int i = 0; i < hands[player].numCardsLeft(); i++)
				{
					if (checkIfPlayable(this.hands[player].getCard(i)))
					{
						skippable = false;
					}
				}
				if (this.playedCards.numCardsLeft() != 0)
				{	
					executeCards(player);
					break;
				}
				else
				{
					if (skippable)
					{
						executeCards(player);
						break;
					}
					else
					{
						System.out.println("\n\033[31mYou have cards to play! Why are you skipping???\033[0m\n");
						input="";
					}
				}
			}
			else if (input.toLowerCase().equals("r"))
			{
				System.out.println("Restarting your turn!\n");
				//Note: there isn't a catch here as based on the code, it should be theoretically impossible to draw non-existant cards here
				this.hands[player].addCards(this.playedCards.drawCards(this.playedCards.numCardsLeft()));
			}
			/*else if (input.toLowerCase().equals("h"))
			{
				printOpponentHands(player);
			}*/
			else if (input.toLowerCase().equals("c"))
			{
				printOpponentCounts(player);
			}
			else if (input.toLowerCase().equals("i"))
			{
				printCommands();
			}
			else if (input.toLowerCase().equals("uno"))
			{
				if (unUsedUNO)
				{
					callUNO(player);
					unUsedUNO=false;
				}
			}
			else
			{
				try
				{
					index = Integer.parseInt(input);
					if (checkIfPlayable(this.hands[player].getCard(index-1)))
					{
						this.playedCards.addCards(this.hands[player].drawCardAtIndex(index-1));
					}
					else
					{
						throw new IllegalArgumentException();
					}
				}
				catch (Exception e)
				{
					System.out.println("\n\033[31mPlease enter a valid input\033[0m\n");
				}
			}
		}
	}
	
	public void callUNO(int callingPlayer)
	{
		if (this.hangingUNO)
		{
			if (callingPlayer != this.hangingPlayer)
			{
				try
				{
					this.hands[this.hangingPlayer].addCards(this.deck.drawCards(5));
				}
				catch (Exception e)
				{
					pileReshuffle();
					this.hands[this.hangingPlayer].addCards(this.deck.drawCards(5));
				}
				System.out.println("\nPlayer " + (this.hangingPlayer+1) + " draws 5 cards for hanging uno!\n");
			}
			this.hangingUNO = false;
		}
		else
		{
			try
			{
				this.hands[callingPlayer].addCards(this.deck.drawCards(2));
			}
			catch (Exception e)
			{
				pileReshuffle();
				this.hands[callingPlayer].addCards(this.deck.drawCards(2));
			}
			System.out.println("\nYou called UNO at the wrong time! Draws 3 cards!\n");
		}
	}
	
	public void checkHanging(int player)
	{
		if (this.hands[player].numCardsLeft() == 1)
		{
			this.hangingUNO = true;
			this.hangingPlayer = player;
		}
	}
	
	public void printOpponentCounts(int player)
	{
		System.out.println("Showing your oppenents' card totals!\n");
		for (int i = 0; i < hands.length; i++)
		{
			if (i != player)
			{
				System.out.println((i+1)+"'s card total: " + hands[i].numCardsLeft());
			}
		}
		System.out.println("Deck card total: " + this.deck.numCardsLeft());
		System.out.println("Pile card total: " + this.pile.numCardsLeft());
		System.out.println("");
	}
	
	public void printOpponentHands(int player)
	{
		System.out.println("Showing your oppenents' hands!\n");
		for (int i = 0; i < this.hands.length; i++)
		{
			if (i != player)
			{
				System.out.println((i+1)+"'s hand:" + this.hands[i]);
			}
		}
		System.out.println("");
	}
	
	public void executeCards(int player)
	{
		checkHanging(player);
		if (this.playedCards.numCardsLeft() == 0)
		{
			System.out.println();
			int amountDrawn=0;
			Card draw;
			do
			{
				try
				{
					draw = this.deck.drawCardAtIndex(deck.numCardsLeft()-1);
				}
				catch (Exception e)
				{
					pileReshuffle();
					draw = this.deck.drawCardAtIndex(deck.numCardsLeft()-1);
				}
				hands[player].addCards(draw);
				amountDrawn+=1;
			} while(!checkIfPlayable(draw));
			
			System.out.println("You drew " + amountDrawn + " cards!\n");
		}
		else
		{
			comboCheck(player);
			for(int i = 0; i < this.playedCards.numCardsLeft(); i++)
			{
				
				if (this.playedCards.getCard(i).getValue() == CardValue.SKIP)
				{
					if (this.getNextTurn() == player)
					{
						System.out.println("Player " +  (this.getNextTurn() + 1) + "'s turn skipped! (that's your own turn bestie...)\n");
					}
					else
					{
						System.out.println("Player " +  (this.getNextTurn() + 1) + "'s turn skipped!\n");
					}
					this.incrementTurn(1);
				}
				else if (this.playedCards.getCard(i).getValue() == CardValue.PLUSTWO)
				{
					try
					{
						this.hands[this.getNextTurn()].addCards(this.deck.drawCards(2));
					}
					catch (Exception e)
					{
						pileReshuffle();
						this.hands[this.getNextTurn()].addCards(this.deck.drawCards(2));
					}
					System.out.println("Player " +  (this.getNextTurn() + 1) + " draws two cards!\n");
				}
				else if (this.playedCards.getCard(i).getValue() == CardValue.REVERSE)
				{
					this.ascendingTurnOrder=!this.ascendingTurnOrder;
					System.out.println("Order reversed!\n");
				}
				
			}
			this.pile.addCards(this.playedCards.drawCards(this.playedCards.numCardsLeft()));
		}
	}
	
	public void pileReshuffle()
	{
		CardList temp = new CardList();
		//move all first pile card to temp
		temp.addCards(this.pile.drawCards(1));
		//move pile cards to deck
		this.deck.addCards(this.pile.drawCards(this.pile.numCardsLeft()));
		//shuffle deck
		this.deck.shuffle();
		//set pile to temp
		this.pile = temp;
		
		System.out.println("\nThe Deck was exhausted! Reshuffling the deck \n");
	}
	
	public void printCommands()
	{
		System.out.println("\033[31mr\033[0m restarts your turn. Useful if you played cards in the wrong order.\n");
		System.out.println("\033[31mc\033[0m shows a count of the amount of cards each of your opponents has left, as well as how many cards are left in the deck and pile.\n");
		System.out.println("Call \033[31muno\033[0m if you have only one card left in hand when you end your turn. If your oppnent calls \033[31muno\033[0m against you when you haven't, you draw five cards. Calling \033[31muno\033[0m in vain draws you three cards.\n");
		System.out.println("\033[31ms\033[0m completes your turn and sends your played combination of cards to the pile. If you have no cards, it draws until you have a card you can play.\n");
	}
	
	public boolean gameWon()
	{
		for (int i = 0; i < this.hands.length; i++)
		{
			if (this.hands[i].numCardsLeft() == 0)
			{
				winner = i;
				return true;
			}
		}
		return false;
	}
	
	public void incrementTurn(int turns)
	{
		if (this.ascendingTurnOrder)
		{
			this.turnPlace += turns;
		}
		else
		{
			this.turnPlace -= turns;
		}
		
		if (this.turnPlace >= this.hands.length)
		{
			this.turnPlace -= (this.hands.length);
		}
		else if (this.turnPlace < 0)
		{
			this.turnPlace += (this.hands.length);
		}
	}
	
	public int getNextTurn()
	{
		int next = this.turnPlace;
		if (this.ascendingTurnOrder)
		{
			next += 1;
		}
		else
		{
			next -= 1;
		}
		
		if (next >= this.hands.length)
		{
			next -= (this.hands.length);
		}
		else if (next < 0)
		{
			next += (this.hands.length);
		}
		
		return next;
	}
	
	public boolean checkIfPlayable(Card played)
	{	
		Card refference;
		if (this.playedCards.numCardsLeft() != 0)
		{
			refference = this.playedCards.getTopCard();
		}
		else
		{
			refference = this.pile.getTopCard();
		}
		
		if (played.getColour() == refference.getColour() ||
			played.getValue() == refference.getValue())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void printOptions(int length)
	{
		String s = "";
		for (int i = 1; i <= length; i++)
		{
			if (i < 10)
			{
				s+="  " + i + "  ";
			}
			else
			{
				s+=" " + i + "  ";
			}
		}
		System.out.println(s);
	}
	
	public int getTurn()
	{
		return this.turnPlace;
	}	
	
	public void comboCheck(int player)
	{
		if (this.playedCards.numCardsLeft() > this.highestCombo)
		{
			this.highestCombo = this.playedCards.numCardsLeft();
			this.highestComboer = player;
		}
	}
	
	public String toString()
	{
		String s = "";
		s+="________________________\n";
		
		s+="This game's stats!\n";
		
		s+= "Player " + (winner+1) + " won this game!\n";
		
		for (int i = 0; i < hands.length; i++)
		{
			if (i != winner)
			{
				s+= "Player "+(i+1)+"'s card total was: " + this.hands[i].numCardsLeft() +"\n";
			}
		}
		
		s+= "This game's highest combo was " + this.highestCombo + " cards long done by player " + (this.highestComboer+1) + "\n";
		
		s+="________________________\n";
		
		return s;
	}
}